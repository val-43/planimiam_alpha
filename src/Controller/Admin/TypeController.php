<?php

namespace App\Controller\Admin;

use App\Entity\Type;
use App\Form\TypeType;
use App\Repository\TypeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TypeController extends AbstractController
{
    /**
     * @Route("/admin/type", name="admin_types")
     */
    public function index(TypeRepository $repo): Response
    {
        $types = $repo->findAll();
        return $this->render('admin/type/adminType.html.twig', [
            "types" => $types
        ]);
    }

    /**
     * @Route("/admin/create", name="ajoutType")
     * @Route("/admin/type/{id}", name="modifType", methods="POST|GET")
     */
    public function ajoutEtModif(Type $type = null, Request $request, EntityManagerInterface $om): Response
    {
        if(!$type){
            $type = new Type();
        }

        $form = $this->createForm(TypeType::class, $type);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $om->persist($type);
            $om->flush();
            $this->addFlash('success', "L'action a bien été réalisée");
            return $this->redirectToRoute("admin_types");
        }

        return $this->render('admin/type/ajoutEtModif.html.twig', [
            "type" => $type,
            "form" => $form->createView()
        ]);
    }

    /**
     * @Route("(/admin/type/{id}", name="supType", methods="delete" )
     */
    public function suppression(Type $type, EntityManagerInterface $entityManager, Request $request): Response
    {
        if($this->isCsrfTokenValid('SUP'.$type->getId(), $request->get('_token'))){
            $entityManager->remove($type);
            $entityManager->flush();
            $this->addFlash('success', "L'action a bien été réalisée");
            return $this->redirectToRoute("admin_types");
        }
    }

}
